#!/bin/bash

echo "[$(date -u)] cd to current script dir"
cd "$(dirname "${BASH_SOURCE[0]}")"

datetimetoday=$(date -u +"%Y%m%d_%H%M")

echo "[$(date -u)] Start container confluence_editor"
docker run -d --name confluence_editor -v confluence-only_confluenceApplicationHome:/var/atlassian/application-data/confluence --network confluence-only_confnet alpine sh -cx 'apk --update add nano vim file mysql-client postgresql-client && sleep infinity'
#echo "[$(date -u)] Stopping confluence"
#docker stop confluence-only-confluence-1
echo "[$(date -u)] Copying confluence data"
docker cp confluence_editor:/var/atlassian/application-data/confluence - | pigz -c -9 >  /root/backups-jira-confluence/backup-confluence-data-${datetimetoday}.tar.gz
echo "[$(date -u)] Copying confluence db"
PGPASSWORD="CHANGEME" docker exec -i confluence-only-confluencedb-1 pg_dump -d db -U confdbuser | pigz -c -9 > /root/backups-jira-confluence/backup-confluence-db-${datetimetoday}.sql.gz
#use -W to ask for password
echo "[$(date -u)] Stopping/removing container confluence_editor"
docker stop confluence_editor
docker rm confluence_editor
echo "[$(date -u)] Backup done, found in ~/backups-jira-confluence folder"
echo "[$(date -u)] Deleting backups older than 7 days"
find /root/backups-jira-confluence/ -type f -mtime +6 -delete