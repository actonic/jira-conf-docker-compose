#!/bin/bash
datetimetoday=$(date -u +"%Y%m%d_%H%M")
echo "Start container confluence_editor"
docker run -d --name confluence_editor -v confluence-only_confluenceApplicationHome:/var/atlassian/application-data/confluence --network confluence-only_confnet alpine sh -cx 'apk --update add nano vim file mysql-client postgresql-client && sleep infinity'
echo "Stopping confluence"
docker stop confluence-only_confluence_1
echo "Copying confluence data"
docker cp confluence_editor:/var/atlassian/application-data/confluence - | pigz -c >  ~/backup-confluence-data-${datetimetoday}.tar.gz
echo "Copying confluence db"
PGPASSWORD="CHANGE_THIS_WITH_YOUR_DB_PASSWORD" docker exec -i confluence-only_confluencedb_1 pg_dump -d db -U confdbuser | pigz -c > ~/backup-confluence-db-${datetimetoday}.sql.gz
#use -W to ask for password
echo "Stopping/removing container confluence_editor"
docker stop confluence_editor
docker rm confluence_editor
echo "Done. Backups are in the home current user directory. Do your edits now and restart confluence"