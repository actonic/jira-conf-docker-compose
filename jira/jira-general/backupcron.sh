#!/bin/bash

echo "[$(date -u)] cd to current script dir"
cd "$(dirname "${BASH_SOURCE[0]}")"

datetimetoday=$(date -u +"%Y%m%d_%H%M")
echo "[$(date -u)] Start container jira_editor"
docker run -d --name jira_editor -v jira-only_jiraApplicationHome:/var/atlassian/application-data/jira --network jira-only_jiranet alpine sh -cx 'apk --update add nano vim file mysql-client postgresql-client && sleep infinity'
#echo "[$(date -u)] Stopping jira"
#docker stop jira
echo "[$(date -u)] Copying jira data"
docker cp jira_editor:/var/atlassian/application-data/jira - | pigz -c -9 >  /root/backups-jira-confluence/backup-jira-data-${datetimetoday}.tar.gz
echo "[$(date -u)] Copying jira db"
PGPASSWORD="CHANGEME" docker exec -i postgresql-for-jira pg_dump -d db -U jiradbuser | pigz -c -9 > /root/backups-jira-confluence/backup-jira-db-${datetimetoday}.sql.gz
#use -W to ask for password
echo "[$(date -u)] Stopping/removing container jira_editor"
docker stop jira_editor
docker rm jira_editor
echo "[$(date -u)] Backup done, found in ~/backups-jira-confluence"
echo "[$(date -u)] Deleting backups older than 7 days"
find /root/backups-jira-confluence/ -type f -mtime +6 -delete