#!/bin/bash
datetimetoday=$(date -u +"%Y%m%d_%H%M")
echo "Start container jira_editor"
docker run -d --name jira_editor -v jira-only_jiraApplicationHome:/var/atlassian/application-data/jira --network jira-only_jiranet alpine sh -cx 'apk --update add nano vim file mysql-client postgresql-client && sleep infinity'
echo "Stopping jira"
docker stop jira
echo "Copying jira data"
docker cp jira_editor:/var/atlassian/application-data/jira - | pigz -c >  ~/backup-jira-data-${datetimetoday}.tar.gz
echo "Copying jira db"
PGPASSWORD="CHANGE_THIS_WITH_YOUR_DB_PASSWORD" docker exec -i postgresql-for-jira pg_dump -d db -U jiradbuser | pigz -c > ~/backup-jira-db-${datetimetoday}.sql.gz
#use -W to ask for password
echo "Stopping/removing container jira_editor"
docker stop jira_editor
docker rm jira_editor
echo "Done. Backups are in the home current user directory. Do your edits now and restart jira"
