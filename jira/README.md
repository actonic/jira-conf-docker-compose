Set of docker recipes for Jira:

* jira-general -> Default deployment to use on life envs
* jira8-mssql -> Jira 8 with Microsoft SQL Server as database
* jira8-mysql -> Jira 8 with MySQL as database
* jira8-oracle -> Jira 8 with Oracle XE as database (other editions are not avaiable without oracle account)
* jira8-postgres -> Jira 8 with PostgreSQL as database
* jira8-postgres-preloaded -> Jira 8 with preloaded dataset ( [link](https://developer.atlassian.com/platform/marketplace/dc-apps-performance-toolkit-user-guide-jira/#option-2--loading-the-dataset-through-xml-import---4-hours-) )


TODO:

* Jira healthcheck ( [link](https://confluence.atlassian.com/jirakb/how-to-retrieve-health-check-results-using-rest-api-867195158.html) )
* Jira reindex ( [link](https://confluence.atlassian.com/jirakb/reindex-jira-server-using-rest-api-via-curl-command-663617587.html) )


References:

* https://hub.docker.com/r/atlassian/jira-software
* https://developer.atlassian.com/platform/marketplace/dc-apps-performance-toolkit-user-guide-jira
