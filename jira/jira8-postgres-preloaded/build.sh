echo "# building"
docker buildx build --tag $(basename `pwd`):8.20.10 .
echo "# to push, run below cmds:"
echo docker tag $(basename `pwd`):8.20.10 vmd84338.contaboserver.net/$(basename `pwd`):8.20.10
echo docker push vmd84338.contaboserver.net/$(basename `pwd`):8.20.10
