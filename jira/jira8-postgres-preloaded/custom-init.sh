#!/bin/bash
set -ue

echo "# Setting vars ..."
JIRA_DB_NAME="jira"
JIRA_DB_USER="jira"
JIRA_DB_PASS="jira"
DB_HOST="localhost"
DB_DUMP_DOWNLOAD="${DB_DUMP_DOWNLOAD:-no}"
DB_DUMP_URL="${DB_DUMP_URL:-https://centaurus-datasets.s3.amazonaws.com/jira/8.20.10/large/db.dump}"
DB_DUMP_PATH="${DB_DUMP_PATH:-/dataset/db.dump}"

echo "# Printing vars ..."
echo "JIRA_DB_NAME=${JIRA_DB_NAME}"
echo "JIRA_DB_USER=${JIRA_DB_USER}"
echo "JIRA_DB_PASS=${JIRA_DB_PASS}"
echo "DB_DUMP_DOWNLOAD=${DB_DUMP_DOWNLOAD}"
echo "DB_DUMP_PATH=${DB_DUMP_PATH}"

echo "# Starting postgresql ..."
/etc/init.d/postgresql start

echo "# Checking postgresql ..."
PGPASSWORD=${JIRA_DB_PASS} pg_isready -U ${JIRA_DB_USER} -h ${DB_HOST}

if [ ! -s "$PGDATA/PG_VERSION" ]; then
  echo "# Checking dump file $DB_DUMP_PATH"
  if [ ! -f "${DB_DUMP_PATH}" ]; then
     if echo "${DB_DUMP_DOWNLOAD}" | grep -iq yes; then
       echo "# Download database dump (that can take a while) ..."
       curl -o ${DB_DUMP_PATH} "${DB_DUMP_URL}"
     else
       echo "# Please download dump and provide it via volume mapping as $DB_DUMP_PATH"
       echo "# or set DB_DUMP_DOWNLOAD=Y to download automatically"
       exit 1
     fi
  fi
  
  echo "# Creating DB user $JIRA_DB_USER ..."
  su -c "psql -c \"create user $JIRA_DB_USER with CREATEDB PASSWORD '$JIRA_DB_PASS';\"" postgres
  su -c "psql -c \"create user atljira       with CREATEDB PASSWORD '$JIRA_DB_PASS';\"" postgres
  
  echo "# Creating empty database ..."
  PGPASSWORD=${JIRA_DB_PASS} dropdb -U ${JIRA_DB_USER} -h ${DB_HOST} ${JIRA_DB_NAME} >/dev/null 2>&1 || echo
  PGPASSWORD=${JIRA_DB_PASS} createdb -U ${JIRA_DB_USER} -h ${DB_HOST} -T template0 -E "UNICODE" -l "C" ${JIRA_DB_NAME}
  
  echo "# Restoring database from $DB_DUMP_PATH (that can take a while) ..."
  touch "${DB_DUMP_PATH}.log"; chmod 777 "${DB_DUMP_PATH}.log"
  PGPASSWORD=${JIRA_DB_PASS} pg_restore --schema=public -v -U ${JIRA_DB_USER} -h ${DB_HOST} -d ${JIRA_DB_NAME} ${DB_DUMP_PATH} >${DB_DUMP_PATH}.log 2>&1 || echo "WARNING: Please check ${DB_DUMP_PATH}.log"
  
  echo ""
  echo "#=======================================================#"
  echo "# IMPORTANT: new admin user credentials are admin/admin #"
  echo "#=======================================================#"
  echo ""
else
  echo "# DB exists already, skipping import of dataset"
fi 

echo "# Checking vars ..."
[ "x" != "${ATL_JDBC_URL:-x}"      ] && echo "# Unsetting ATL_JDBC_URL      (as using preloaded db instead) ..."
[ "x" != "${ATL_JDBC_USER:-x}"     ] && echo "# Unsetting ATL_JDBC_USER     (as using preloaded db instead) ..."
[ "x" != "${ATL_JDBC_PASSWORD:-x}" ] && echo "# Unsetting ATL_JDBC_PASSWORD (as using preloaded db instead) ..."
[ "x" != "${ATL_DB_DRIVER:-x}"     ] && echo "# Unsetting ATL_DB_DRIVER     (as using preloaded db instead) ..."
[ "x" != "${ATL_DB_TYPE:-x}"       ] && echo "# Unsetting ATL_DB_TYPE       (as using preloaded db instead) ..."

echo "# Starting Jira ..."
export ATL_JDBC_URL=jdbc:postgresql://localhost:5432/jira
export ATL_JDBC_USER=jira
export ATL_JDBC_PASSWORD=jira
export ATL_DB_DRIVER=org.postgresql.Driver
export ATL_DB_TYPE=postgres72
/entrypoint.py
