## Oracle DB setup
1. Make sure you have an oracle account (it's free, just go to oracle.com and create one)

2. Login to Oracle registry `docker login container-registry.oracle.com`

3. Download images `docker compose pull`

4. Check port mapping

5. Start db `docker compose up -d db; docker compose logs -f`

6. Wait till `DATABASE IS READY TO USE!` in the log (db can take up to 30mins for the first start)

7. Start jira `docker compose up -d jira`

8. Fix dbconfig.xml `docker compose exec jira sed -i -e 's/>select 1</>select 1 from dual</g' dbconfig.xml`

9. Monitor log till Jira is ready `docker compose logs -f --tail=100`
