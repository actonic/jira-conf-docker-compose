while ! echo show pdbs | sqlplus -s / as sysdba | grep WRITE; do sleep 1; done

echo "# Creating DB user $DB_USER ..."
echo "
set echo on
alter session set container = $ORACLE_PDB;
create user $DB_USER identified by $DB_PASS default tablespace USERS quota unlimited on USERS;
grant connect         to $DB_USER;
grant create table    to $DB_USER;
grant create sequence to $DB_USER;
grant create trigger  to $DB_USER;
" | sqlplus -s / as sysdba
